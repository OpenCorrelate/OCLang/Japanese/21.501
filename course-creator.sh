#! /bin/sh

create () {
    local root=$1
    local x=$2
    local y=$3

    for i in $(seq -f "%02g" $x $y)
    do
      mkdir -p ${root}${i}/{scratch,exercises}
      touch ${root}${i}/{README.md,exercises/README.md}
    done

}
create "texts/genki01-speaking/lesson" 1 12
create "texts/genki01-rw/lesson" 1 12
create "video/genki-wojnovich/lesson" 1 9
create "video/genki-wojnovich/hiragana" 1 6
create "video/genki-wojnovich/katakana" 1 6
create "video/genki-wojnovich/verb" 1 4
create "video/genki-wojnovich/verb" 1 3
