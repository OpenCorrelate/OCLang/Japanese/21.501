# Basic Grammar 1

## Particles

* **は** denotes subject of the sentence
    - (たかしさん　は　にほんご　です。)
        * **ろまじ**: *Takashi-san wa Nihongo desu.*
        * **えいご**: *Takashi-san is Japanese.*
    - (おれ　は　あめりかじん　です。)
        * **ろまじ**: *Ore wa Amerikajin desu.*
        * **えいご**: *I am an American.*
    - (ほん　は　あかい　です)
        * **ろまじ**: *Hon wa akai desu.*
        * **えいご**: *The book is red.*
